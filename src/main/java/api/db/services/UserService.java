package api.db.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import api.db.interfaces.IUserRepository;
import api.db.models.User;

@Service
public class UserService {
	@Autowired
	private IUserRepository UserRepository;
	
	public Optional<User> getUser(int id) {
		return UserRepository.findById(id);
	}
	
	public User setUser(User User) {
		return UserRepository.save(User);
	}
	
	public  User updateUser(int id, User User) {
		UserRepository.deleteById(id);
		return UserRepository.save(User);
	}
	
	public void deleteUser(int id) {
		UserRepository.deleteById(id);
	}
	
}
