package api.db.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import api.db.interfaces.IAddressRepository;
import api.db.models.Address;

@Service
public class AddressService {
	@Autowired
	private IAddressRepository addressRepository;
	
	public Optional<Address> getAddress(int id) {
		return addressRepository.findById(id);
	}
	
	public Address setAddress(Address address) {
		return addressRepository.save(address);
	}
	
	public  Address updateAddress(int id, Address address) {
		addressRepository.deleteById(id);
		return addressRepository.save(address);
	}
	
	public void deleteAddress(int id) {
		addressRepository.deleteById(id);
	}

}
