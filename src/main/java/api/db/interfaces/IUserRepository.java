package api.db.interfaces;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import api.db.models.User;

@Repository
public interface IUserRepository extends CrudRepository<User, Integer> {

}
