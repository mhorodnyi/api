package api.db.interfaces;

import org.springframework.data.repository.CrudRepository;

import api.db.models.Address;

public interface IAddressRepository extends CrudRepository<Address, Integer> {

}
