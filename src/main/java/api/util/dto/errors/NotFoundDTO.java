package api.util.dto.errors;

import org.springframework.stereotype.Component;

import lombok.Getter;

@Getter
@Component
public class NotFoundDTO {
	private String message;
	
	public NotFoundDTO()
	{
		message = "Requested item was not find";
	}
}
