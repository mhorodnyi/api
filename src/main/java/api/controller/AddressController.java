package api.controller;

import java.util.NoSuchElementException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import api.db.models.Address;
import api.db.services.AddressService;
import api.util.dto.AddressDTO;
import api.util.dto.errors.NotFoundDTO;

@Controller
@RequestMapping(path="/address")
public class AddressController {
	@Autowired
	private AddressService addressService;
	
	@Autowired
	private ModelMapper mapper;
	
	@Autowired
	private NotFoundDTO notFound;
	
	@GetMapping("/{id}")
	public @ResponseBody AddressDTO getAddress(@PathVariable("id") int id) {
		return mapper.map(addressService.getAddress(id).get(), AddressDTO.class);
	}
	
	@PostMapping("/")
	public @ResponseBody AddressDTO setAddress(@RequestBody AddressDTO address) {
		return mapper.map(
				addressService.setAddress(mapper.map(address, Address.class)), 
				AddressDTO.class);
	}
	
	@PutMapping("/{id}")
	public @ResponseBody AddressDTO updateAddress(@PathVariable("id") int id, @RequestBody AddressDTO address) {
		return mapper.map(
				addressService.updateAddress(id, mapper.map(address, Address.class)), 
				AddressDTO.class);
	}
	
	@DeleteMapping("/{id}")
	public void deleteAddress(@PathVariable("id") int id) {
		addressService.deleteAddress(id);
	}
	
	@ExceptionHandler({NoSuchElementException.class, EmptyResultDataAccessException.class, HttpMessageNotReadableException.class})
	public ResponseEntity<NotFoundDTO> handleNotFoundException() {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(notFound);
	}
}
