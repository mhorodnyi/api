package api.controller;

import java.util.NoSuchElementException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import api.db.models.User;
import api.db.services.UserService;
import api.util.dto.UserDTO;
import api.util.dto.errors.NotFoundDTO;

@RestController
@RequestMapping(path="/user")
public class UserController {
	@Autowired
	private UserService userService;
	
	@Autowired
	private ModelMapper mapper;
	
	@Autowired
	private NotFoundDTO notFound;
	
	@GetMapping("/{id}")
	public UserDTO getUser(@PathVariable("id") int id) {
		return mapper.map(userService.getUser(id).get(), UserDTO.class);
	}
	
	@PostMapping("/")
	public UserDTO setUser(@RequestBody UserDTO user) {
		return mapper.map(
				userService.setUser(mapper.map(user, User.class)), 
				UserDTO.class);
	}
	
	@PutMapping("/{id}")
	public UserDTO updateUser(@PathVariable("id") int id, @RequestBody UserDTO user) {
		return mapper.map(
				userService.updateUser(id, mapper.map(user, User.class)), 
				UserDTO.class);
	}
	
	@DeleteMapping("/{id}")
	public void deleteUser(@PathVariable("id") int id) {
		userService.deleteUser(id);
	}
	
	@ExceptionHandler({NoSuchElementException.class, EmptyResultDataAccessException.class, HttpMessageNotReadableException.class})
	public ResponseEntity<NotFoundDTO> handleNotFoundException() {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(notFound);
	}
}
